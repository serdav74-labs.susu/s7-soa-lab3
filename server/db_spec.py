from pony.orm import *
import os

db = Database()

class User(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str, unique=True)
    total_turns = Required(int, default=0)
    total_matches = Required(int, default=0)
    matches_won = Required(int, default=0)

    @property
    def winrate(self) -> float:
        return float(self.matches_won) / self.total_matches if self.total_matches > 0 else 0.0

class City(db.Entity):
    id = PrimaryKey(int)
    region_id = Required(int)
    name = Required(str)

def bind_db(db, host='db'):
    user = os.environ.get('POSTGRES_USER')
    password = os.environ.get('POSTGRES_PASSWORD')
    db_name = os.environ.get('POSTGRES_DB')

    db.bind(provider='postgres', host=host, user=user, password=password, database=db_name)
    db.generate_mapping(create_tables=True)

bind_db(db)