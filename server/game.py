from enum import IntEnum
from random import shuffle
from datetime import datetime, timedelta
from threading import Thread

import logging

from time import sleep

from db_spec import db_session, User, City

class UserState(IntEnum):
    IN_LOBBY = 1
    QUEUED = 2
    IN_GAME = 3
    SPECTATING = 4
    OFFLINE = 100

"""
Класс, содержащий информацию о ходе пользователя.
"""
class Turn:
    def __init__(self, id, player, city):
        self.id = id
        self.player = player
        self.city = city
        self.datetime = datetime.now()
    
    def to_dict(self):
        return {
            'id': self.id,
            'player': self.player,
            'city': self.city,
            'ts': self.datetime.timestamp()
        }

"""
Синглтон для управления играми, "Game master".
"""
class GlobalGameState:
    gm_logger = logging.getLogger('app.GM')
    next_game_id = 0
    shard_name = 'Main server'
    turn_timeout = 30
    game_expiration_timeout = 60
    purge_games = False
    games = dict()

    # количество игроков в одной игре
    game_size = 3

    matchmaking_queue = list()

    """
    Создаёт новую игру с указанными игроками.
    :returns Объект класса Game
    """
    @classmethod
    def make_game(cls, players):
        cls.next_game_id += 1

        id = cls.next_game_id - 1
        game = GameState(id=id, players=players)

        cls.games[id] = game

        return game
    
    """
    Ищет пользователя и возвращает его статус.
    """
    @classmethod
    def get_user_state(cls, user):
        if user in cls.matchmaking_queue:
            return { 'state': UserState.QUEUED, 'queue_length': len(cls.matchmaking_queue) }
        else: 
            game = next(filter(lambda game: user in game.players or user in game.spectators, cls.games.values()), None)
            if game:
                if user in game.players:
                    return { 'state': UserState.IN_GAME, 'game_id': game.id }
                else:
                    return { 'state': UserState.SPECTATING, 'game_id': game.id }
            else:
                return { 'state': UserState.IN_LOBBY }

    
    # Гейм-мастер управляет играми и очередью в отдельном потоке.
    # Эта переменная указывает, что поток должен завершить работу.
    game_master_halt_flag = False

    # Созданный объект класса threading.Thread
    game_master_thread = None
    
    """
    Функция, которая выполняется в потоке гейм-мастера. Управляет очередью и идущими играми.
    """
    @classmethod
    def game_master_loop(cls):
        with db_session:
            while not cls.game_master_halt_flag:
                sleep(0.5)
                cls.matchmaking_job()
                cls.tick_games()
    
    """
    Распределяет игроков из очереди матчмейкинга по играм.
    """
    @classmethod
    def matchmaking_job(cls):
        while len(cls.matchmaking_queue) >= cls.game_size:
            cls.gm_logger.info('Preparing a game...')
            players = list()
            while len(players) < cls.game_size:
                players.append(cls.matchmaking_queue.pop(0))
            
            game = cls.make_game(players)

            cls.gm_logger.info(f'Created new game (id:{game.id}) with players: {", ".join(player.name for player in players)}')
    
    """
    Тикает идущие игры и удаляет завершённые.
    """
    @classmethod
    def tick_games(cls):
        expired_games = []

        for game_id, game in cls.games.items():
            game.tick()
            if cls.purge_games:
                if game.complete and datetime.now() > game.delete_after:
                    expired_games.append(game_id)
                    game.spectators.clear()
                    game.players.clear()
        
        for game_id in expired_games:
            cls.games.pop(game_id)

    
    @classmethod
    def start_game_master(cls):
        cls.gm_logger.info('Starting new thread for GM loop...')
        cls.game_master_thread = Thread(target=cls.game_master_loop)
        cls.game_master_thread.start()
    
    @classmethod
    def stop_game_master(cls):
        cls.gm_logger.info('Waiting for GM loop to finish...')
        cls.game_master_halt_flag = True
        cls.game_master_thread.join()
        cls.gm_logger.info('GM thread joined.')
            

"""
Класс для описания отдельной игры.
"""
class GameState:
    def __init__(self, id, players):
        self.id = id

        self.complete = False

        self.logger = logging.getLogger('app.game')
        
        self.players = players[:]
        shuffle(self.players)

        self.spectators = []

        self.delete_after = datetime.now()

        self.previous_city = None
        self.cities_taken = set()
        self.current_turn = 0
        self.turn_end = datetime.now() + timedelta(seconds=GlobalGameState.turn_timeout + 2)

        self.turn_history = list()
        self.next_turn_id = 0
    
    """
    Обрабатывает ход игрока.
    """
    def advance(self, player, city):
        if player in self.players:
            if player == self.players[self.current_turn]:
                if self.check_city(city):
                    self.handle_successful_turn(player, city)
            else:
                raise InvalidTurnError("Сейчас не ваш ход!")
        else:
            raise WrongGameError(f"Игрок {player.name} не участвует в игре {self.id}!")
    
    """
    Тикает игру.
    """
    def tick(self):
        if self.complete:
            return
        
        if datetime.now() < self.turn_end:
            pass
        else:
            self.handle_knockout()
    
    """
    Добавляет ход в историю ходов. Допускаются системные сообщения.
    """
    def append_history(self, username, city):
        self.turn_history.append(
            Turn(
                self.next_turn_id,
                username,
                city
            )
        )
        self.next_turn_id += 1
    
    """
    Обработка успешного хода.
    """
    def handle_successful_turn(self, player, city):
        self.cities_taken.add(city)
        self.append_history(player.name, city)

        self.previous_city = city
        self.current_turn += 1
        if self.current_turn >= len(self.players):
            self.current_turn = 0
        
        self.turn_end = datetime.now() + timedelta(seconds=GlobalGameState.turn_timeout + 2)

        with db_session:
            player.total_turns += 1
    
    """
    Обработка выбывания игрока.
    """
    def handle_knockout(self):
        loser = self.players.pop(self.current_turn)
        self.logger.info(f"Kicked {loser.name} from game (turn timeout).")
        self.append_history("Система", f"{loser.name} выбывает из игры - время хода вышло.")

        self.spectators.append(loser)

        if self.current_turn >= len(self.players):
            self.current_turn = 0
        
        loser.total_matches += 1

        if len(self.players) == 1:
            winner = self.players.pop()
            self.logger.info(f"Last standing: {winner.name}. Congratulations.")
            self.append_history("Система", f"{winner.name} одерживает победу (остальные игроки выбыли). Поздравляем!")
            winner.total_matches += 1
            winner.matches_won += 1

            self.spectators.append(winner)
            
            self.delete_after = datetime.now() + timedelta(
                seconds=GlobalGameState.game_expiration_timeout
            )
            self.complete = True

        self.turn_end = datetime.now() + timedelta(seconds=GlobalGameState.turn_timeout + 2)
    
    """
    Проверяет, можно ли сходить данным городом в текущем состоянии игры.
    """
    def check_city(self, city):
        if len(city) == 0:
            return False
        
        prev = self.previous_city

        # определение последней буквы
        if prev is None:
            starting_letter = city[0] # костыль на первый ход
        elif prev[-1] in ('ъ', 'ы', 'ь'):
            starting_letter = prev[-2]
        else:
            starting_letter = prev[-1]
        
        if starting_letter == city[0]:
            if city not in self.cities_taken:
                with db_session:
                    if City.exists(name=city):
                        return True
                    else:
                        raise InvalidTurnError("Такого города не существует!")
            else:
                raise InvalidTurnError("Такой город уже называли!")
        else:
            raise InvalidTurnError(f"Название города должно начинаться с буквы '{starting_letter}'!")
    
    def to_dict(self):
        return {
            'id': self.id,
            'turn_history': [turn.to_dict() for turn in self.turn_history],
            'current_player': None if self.complete else self.players[self.current_turn].name,
            'previous_city': self.previous_city,
            'completed': self.complete
        }
    
    
        
class GameError(Exception):
    pass

class InvalidTurnError(GameError):
    pass

class TurnTimeoutError(GameError):
    pass

class WrongGameError(GameError):
    pass

