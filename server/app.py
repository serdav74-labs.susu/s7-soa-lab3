from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple

from game import GlobalGameState, User, UserState, GameError
from pony.orm import flush, db_session

from jsonrpc import JSONRPCResponseManager as ResponseManager, dispatcher
import logging
import sys

"""
Функция для настройки логгеров.
"""
def setup_logging(level):
    wz_logger = logging.getLogger('werkzeug')
    wz_logger.setLevel(logging.WARNING)

    logger = logging.getLogger('app')
    handler = logging.StreamHandler(stream=sys.stdout)
    formatter = logging.Formatter(
        fmt="{asctime:>16} | {name:16} | {levelname:7} | {message}",
        style="{"
    )

    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger

logger = setup_logging('DEBUG')


"""
Вход в систему (по username). Создаёт пользователя в БД, если он входит впервые.
"""
@dispatcher.add_method
def log_in(username: str):
    logger.debug(f"{username} is logging in...")
    with db_session:
        user = User.get(name=username)
        if user is None:
            logger.debug(f"Creating DB entry for {username}...")
            user = User(name=username)
            flush()
        logger.info(f"{username} has connected.")
        return { 'id': user.id, 'shard_name': GlobalGameState.shard_name }

"""
Помещает пользователя в очередь для создания игры.
"""
@dispatcher.add_method
def enqueue(username: str):
    with db_session:
        user = User.get(name=username)

        logger.debug(f"{username} wants to join matchmaking queue...")
        state_data = GlobalGameState.get_user_state(user)
        if state_data['state'] == UserState.IN_LOBBY:
            GlobalGameState.matchmaking_queue.append(user)
            logger.info(f"{username} has joined matchmaking queue. {len(GlobalGameState.matchmaking_queue)} players are waiting for a match.")
        return GlobalGameState.get_user_state(user)

"""
Получает текущий статус пользователя (в лобби, в очереди или в игре)
"""
@dispatcher.add_method
def get_state(username: str):
    with db_session:
        user = User.get(name=username)

        return GlobalGameState.get_user_state(user)

"""
Получает статус игры.
"""
@dispatcher.add_method
def get_game_state(game_id: int):
    if game_id in GlobalGameState.games:
        return GlobalGameState.games[game_id].to_dict()
    else:
        raise KeyError(f"Game with id:{game_id} not found.")

"""
Делает ход от имени пользователя.
"""
@dispatcher.add_method
def make_turn(username: str, game_id: int, data: str):
    if game_id in GlobalGameState.games:
        game = GlobalGameState.games[game_id]
    else:
        raise KeyError(f"Game with id:{game_id} not found.")
    
    with db_session:
        user = User.get(name=username)

        try:
            game.advance(user, data.strip().casefold())
            return { 'success': True }
        except GameError as e:
            return { 'success': False, 'message': str(e) }

"""
Удаляет пользователя из игры, если он в ней уже не участвует.
"""
@dispatcher.add_method
def leave_game(username: str, game_id: int):
    if game_id in GlobalGameState.games:
        game = GlobalGameState.games[game_id]
    else:
        raise KeyError(f"Game with id:{game_id} not found.")
    
    with db_session:
        user = User.get(name=username)

        if user in game.players:
            pass
        elif user in game.spectators:
            game.spectators = list(filter(lambda u: u.id != user.id, game.spectators))

"""
Регистрация хэндлеров.
"""
@Request.application
def application(request):
    # dispatcher["echo"] = lambda s: s
    
    response = ResponseManager.handle(
        request.data, dispatcher
    )

    return Response(response.json, mimetype='application/json')


# Точка входа в приложение.

if __name__ == '__main__':
    try:
        with db_session:
            GlobalGameState.start_game_master()
            run_simple('', 14300, application)
    except KeyboardInterrupt:
        logger.info('KeyboardInterrupt received.')
        raise
    finally:
        logger.info('Shutting down...')
        GlobalGameState.stop_game_master()
