import requests
import json
from enum import IntEnum
from time import sleep

class PlayerState(IntEnum):
    IN_LOBBY = 1
    QUEUED = 2
    IN_GAME = 3
    SPECTATING = 4
    OFFLINE = 100

class Client:
    def __init__(self):
        self.call_id = 0
        self.server_url = "http://localhost:4000/jsonrpc"

        self.username = None
        self._state = None
        self.game = None

        self.last_logged_turn = -1
    
    @property
    def state(self):
        return self._state
    
    @state.setter
    def state(self, value):
        if self._state != value:
            prev_state = self._state
            self._state = value
            # change the state *before* calling a handler
            self.handle_state_change(prev_state, self._state)
        else:
            self._state = value

    @property
    def turn_expected(self):
        if self.state == PlayerState.IN_GAME and self.game:
            if self.game['current_player'] == self.username:
                return True
        return False
    
    def main_loop(self):
        self.server_url = input('host: ')
        # self.remote_call('dummy')
        self.username = input('username: ')

        data = self.remote_call('log_in', username=self.username)
        print(f"Добро пожаловать на сервер {data['shard_name']}! Ваш id: {data['id']}.")
        
        while True:
            try:
                self.refresh_state()

                if self.state == PlayerState.IN_LOBBY:
                    input("Нажмите Enter, чтобы найти игру.")

                    data = self.remote_call('enqueue', username=self.username)
                    print(f"Вы встали в очередь на игру. Длина очереди: {data['queue_length']} игроков.")

                if self.turn_expected:
                    if self.game['previous_city']:
                        tooltip = f"Ваш ход! {self.game['previous_city']} -> "
                    else:
                        tooltip = f"Вы начинаете игру! Введите любой город: "
                    
                    data = input(tooltip)

                    result = self.remote_call(
                        'make_turn', 
                        username=self.username, 
                        game_id=self.game['id'],
                        data=data
                    )

                    if not result['success']:
                        print(result['message'])

                sleep(0.5)
            except KeyboardInterrupt:
                if self.state == PlayerState.SPECTATING:
                    self.remote_call('leave_game', username=self.username, game_id=self.game['id'])
                else:
                    print("Breaking game loop...")
                    break

    def handle_state_change(self, before, after):
        if before in (PlayerState.QUEUED, PlayerState.IN_LOBBY) and after == PlayerState.IN_GAME:
            print(f'Игра началась!')
        elif before == PlayerState.IN_GAME and after == PlayerState.SPECTATING:
            print(f'Вы больше не участвуете в игре, но можете наблюдать за её ходом. Нажмите Ctrl+C, чтобы выйти в лобби.')
        elif before == PlayerState.SPECTATING and after == PlayerState.IN_LOBBY:
            print(f'Вы вышли в лобби.')
        
    def refresh_state(self):
        data = self.remote_call('get_state', username=self.username)
        self.state = data['state']
        # print(self.state)
        if 'game_id' in data:
            self.refresh_game_state(game_id=data['game_id'])
        else:
            self.game = None
    
    def refresh_game_state(self, game_id):
        data = self.remote_call('get_game_state', game_id=game_id)
        self.game = data

        new_turns = filter(
            lambda turn: turn['id'] > self.last_logged_turn,
            self.game['turn_history']
        )
        for turn in new_turns:
            print(f"{turn['id']:>3} | {turn['player']}: {turn['city']}")
            self.last_logged_turn = max(self.last_logged_turn, turn['id'])
        

    def remote_call(self, method, **kwargs):
        url = self.server_url
        headers = {'content-type': 'application/json'}

        payload = {
            'method': method,
            'jsonrpc': '2.0',
            'id': self.call_id
        }
        self.call_id += 1

        payload['params'] = kwargs

        response = requests.post(
            url,
            data=json.dumps(payload),
            headers=headers
        ).json()
        if 'result' in response:
            return response['result']
        else:
            print(response)
            raise RuntimeError()



def main():
    client = Client()
    client.main_loop()

if __name__ == '__main__':
    main()
